#!/bin/sh

# $1 : adresse IP ou nom de domaine
# $2 : port d'écoute du reverse proxy
# $3 : nom du serveur de base de données

TEMOIN=0
fin=`date +"%s"`
fin=$((fin+300))
while ([ $TEMOIN -eq 0 ] &&  [ `date +"%s"` -lt $fin ]); do
   mysql -h $3 -u userKB -p$DB_PASS kanboard -e "SELECT 1";
   if [ $? -eq 1 ]; then
   sleep 5
   else
       TEMOIN=1
       echo "SUCCESS, MySQL est prêt pour la suite"
   fi
done

if [ $TEMOIN -eq 0 ]; then
   echo "ERREUR, MySQL non encore actif"
   exit 666
fi


ANCIENNE_URL=`mysql -h $3 -u userKB -p$DB_PASS kanboard -e "select value from settings where option='application_url';" -N -s`
ANCIENNE_BASE=`mysql -h $3 -u userKB -p$DB_PASS kanboard -e "select value from settings where option='application_url';" -N -s | cut -d"/" -f4`

# Le répertoire d'accueil du site n'est pas un volume, son contenu est donc effacé à chaque redémarrage

# Création du lien
   ln -snfT /var/www/app/ /var/www/app/$VIRTUAL_HOST

# Mise à jour de Nginx en partant d'un fichier non encore modifié
   sed -i "s/try_files \$uri \$uri\/ \/index.php\$is_args\$args;/try_files \$uri \$uri\/ \/$VIRTUAL_HOST\/index.php;/g" /etc/nginx/nginx.conf
   nginx -s reload
   

# Mise à jour de Nginx si le fichier a déjà été modifié par une précédente base
   sed -i "s/try_files \$uri \$uri\/ \/$ANCIENNE_BASE\/index.php;/try_files \$uri \$uri\/ \/$VIRTUAL_HOST\/index.php;/g" /etc/nginx/nginx.conf
  
   # Mise à jour de MySQL
   mysql -h $3 -u userKB -p$DB_PASS kanboard -e "update settings s set s.value = 'http://$1:$2/$VIRTUAL_HOST/' where s.option = 'application_url';";
   

if [ ! "http://$1:$2/$VIRTUAL_HOST/" = "$ANCIENNE_URL" ]; then
   echo "La nouvelle URL est http://$1:$2/$VIRTUAL_HOST/, il faut la changer dans MySQL"    
  
   # Mise à jour de MySQL
   mysql -h $3 -u userKB -p$DB_PASS kanboard -e "update settings s set s.value = 'http://$1:$2/$VIRTUAL_HOST/' where s.option = 'application_url';";
   
   echo "La nouvelle URL est maintenant http://$1:$2/$VIRTUAL_HOST/"
   else
      echo "L'ancienne URL est $ANCIENNE_URL"
      echo "La nouvelle URL est http://$1:$2/$VIRTUAL_HOST/"
      echo "Il n'y a donc pas de changement d'URL."
fi

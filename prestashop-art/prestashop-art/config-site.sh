#!/bin/sh

# $1 : adresse IP ou nom de domaine
# $2 : port d'écoute du reverse proxy
# $3 : nom du serveur de base de données

TEMOIN=0
fin=`date +"%s"`
fin=$((fin+300))
while ([ $TEMOIN -eq 0 ] &&  [ `date +"%s"` -lt $fin ]); do
   mysql -h $3 -u userPS -p$DB_PASS prestashop -e "SELECT 1";
   if [ $? -eq 1 ]; then
   sleep 5
   else
       TEMOIN=1
       echo "SUCCESS, MySQL est prêt pour la suite"   

       
       # Mise à jour du fichier de paramètres avec le bon mot de passe
       sed -i "s/.*database_password.*/    'database_password' => '$DB_PASS',/g" /var/www/html/app/config/parameters.php

       ANCIENNE_URL=`cat /var/www/html/.htaccess | grep ^#Domain | cut -d" " -f2`
       ANCIENNE_BASE=`mysql -h $3 -u userPS -p$DB_PASS prestashop -e "SELECT physical_uri FROM ps_shop_url;" -N -s`
       
       if [ "$ANCIENNE_URL$ANCIENNE_BASE" != "$1:$2/$VIRTUAL_HOST/" ]; then
         
          # Un peu de ménage
          mysql -h $3 -u userPS -p$DB_PASS prestashop -e "TRUNCATE ps_linksmenutop;"
          mysql -h $3 -u userPS -p$DB_PASS prestashop -e "TRUNCATE ps_connections;"
          mysql -h $3 -u userPS -p$DB_PASS prestashop -e "TRUNCATE ps_connections_source;"
          mysql -h $3 -u userPS -p$DB_PASS prestashop -e "TRUNCATE ps_linksmenutop_lang;"
          mysql -h $3 -u userPS -p$DB_PASS prestashop -e "TRUNCATE ps_log;"
       
          # Suppression du cache
           rm -rf /var/www/html/var/cache/prod
          
          # Modification du fichier .htaccess            
          sed -ie "s/$ANCIENNE_URL/$1:$2/g" /var/www/html/.htaccess
          
          # Mise à jour du domaine dans MySQL
          mysql -h $3 -u userPS -p$DB_PASS prestashop -e "UPDATE ps_shop_url set domain='$1:$2';"
          
          # Création du lien et mise à jour de MySQL si l'ancienne base est différente de la nouvelle         
          if [ "$ANCIENNE_BASE" != "/$VIRTUAL_HOST/" ]; then
             ln -snfT /var/www/html/ /var/www/html/$VIRTUAL_HOST        
             mysql -h $3 -u userPS -p$DB_PASS prestashop -e "UPDATE ps_shop_url set physical_uri='/$VIRTUAL_HOST/';"
             
             if [ "$ANCIENNE_BASE" != "/" ]; then 
                ANCIENNE_BASE=`echo $ANCIENNE_BASE | cut -d"/" -f2`
                sed -ie "s/$ANCIENNE_BASE/$VIRTUAL_HOST/g" /var/www/html/.htaccess
                rm /var/www/html/$ANCIENNE_BASE
                          
                else
                  sed -ie "s/E=REWRITEBASE:\//E=REWRITEBASE:\/$VIRTUAL_HOST\//g" /var/www/html/.htaccess
                  sed -ie "s/ErrorDocument 404 \/index.php?controller=404/ErrorDocument 404 \/$VIRTUAL_HOST\/index.php?controller=404/g" /var/www/html/.htaccess
             fi
           fi
         echo "La nouvelle URL est http://$1:$2/$VIRTUAL_HOST/"
         echo "Le fichier .htaccess a été regénéré."
          
       else
          echo "L'URL http://$1:$2/$VIRTUAL_HOST/ n'a pas changé."
       fi
   fi
done

if [ $TEMOIN -eq 0 ]; then
   echo "ERREUR, MySQL non encore actif."
   exit 666
fi

#!/bin/sh

# $1 : adresse IP ou nom de domaine
# $2 : port d'écoute du reverse proxy
# $3 : nom du serveur de base de données

TEMOIN=0
fin=`date +"%s"`
fin=$((fin+300))
while ([ $TEMOIN -eq 0 ] &&  [ `date +"%s"` -lt $fin ]); do
   mysql -h $3 -u userWP -p$DB_PASS wordpress -e "SELECT 1";
   if [ $? -eq 1 ]; then
   sleep 5
   else
       TEMOIN=1
       echo "SUCCESS, MySQL est prêt pour la suite"
   fi
done

if [ $TEMOIN -eq 0 ]; then
   echo "ERREUR, MySQL non encore actif"
   exit 666
fi

# Mise à jour du wp-config
sed -i "s/.*DB_PASSWORD.*/define( 'DB_PASSWORD', '$DB_PASS');/g" /var/www/html/wp-config.php

ANCIENNE_URL=`wp option get siteurl | cut -d"/" -f3,4`
ANCIENNE_BASE=`wp option get siteurl | cut -d"/" -f4`

if [ ! "$1:$2/$VIRTUAL_HOST" = "$ANCIENNE_URL" ]; then
   echo "La nouvelle URL est http://$1:$2/$VIRTUAL_HOST/, il faut la changer"
   
    # Suppression éventuelle de l'ancien lien
   if [ "$ANCIENNE_BASE" != "" ]; then   
      rm /var/www/html/$ANCIENNE_BASE
   fi   
   
   # Création du nouveau lien
   ln -snfT /var/www/html/ /var/www/html/$VIRTUAL_HOST
   
   wp option update siteurl http://$1:$2/$VIRTUAL_HOST/
   wp option update home http://$1:$2/$VIRTUAL_HOST/
   wp search-replace $ANCIENNE_URL $1:$2/$VIRTUAL_HOST
   NOUVELLE_URL=`wp option get siteurl | cut -d"/" -f3,4`
   echo La nouvelle URL est maintenant http://$NOUVELLE_URL/
   else
      echo "L'ancienne URL est http://$ANCIENNE_URL/"
      echo "La nouvelle URL est http://$1:$2/$VIRTUAL_HOST/"
      echo "Il n'y a donc pas de changement d'URL"
fi

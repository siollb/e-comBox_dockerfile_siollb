#!/bin/sh

if [ ! -e "/var/www/html/.base" ]; then
   echo "Installation d'origine : pas d'ancienne base URL"
   echo "La nouvelle URL contient le suffixe $VIRTUAL_HOST"
   ln -snfT /var/www/html/ /var/www/html/$VIRTUAL_HOST
   echo $VIRTUAL_HOST > /var/www/html/.base
   else
     echo "Rien à faire, il s'agit d'un redémarrage, le lien est déjà créé."    
fi

  



#!/bin/sh

ANCIENNE_BASE=$(cat /etc/nginx/nginx.conf | grep "try_files \$uri \$uri" | cut -d""/"" -f3 | cut -d"?" -f1)

if [ "$ANCIENNE_BASE" = "index.php" ]; then
   echo "Installation d'origine : pas d'ancienne base URL"
   echo "La nouvelle URL contient le suffixe $VIRTUAL_HOST"
   ln -snfT /var/www/localhost/htdocs/ /var/www/localhost/htdocs/$VIRTUAL_HOST
   # Mise à jour de Nginx en partant d'un fichier non encore modifié
   sed -i "s/try_files \$uri \$uri\/ \/index.php?\$args;/try_files \$uri \$uri\/ \/$VIRTUAL_HOST\/index.php;/g" /etc/nginx/nginx.conf
fi

if [ "$ANCIENNE_BASE" != "index.php" ]; then
   # Suppression éventuelle de l'ancien lien et mise à jour de nginx
   if [ "$ANCIENNE_BASE" != "$VIRTUAL_HOST" ]; then
      echo "L'ancienne URL contenait le suffixe $ANCIENNE_BASE"
      echo "La nouvelle URL contient le suffixe $VIRTUAL_HOST"
      rm /var/www/localhost/htdocs/$ANCIENNE_BASE
      ln -snfT /var/www/localhost/htdocs/ /var/www/localhost/htdocs/$VIRTUAL_HOST
      # Mise à jour de Nginx si le fichier a déjà été modifié par une précédente base
      sed -i "s/try_files \$uri \$uri\/ \/$ANCIENNE_BASE\/index.php;/try_files \$uri \$uri\/ \/$VIRTUAL_HOST\/index.php;/g" /etc/nginx/nginx.conf
   fi
fi

  



-- MySQL dump 10.17  Distrib 10.3.20-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: humhub
-- ------------------------------------------------------
-- Server version	10.3.20-MariaDB-1:10.3.20+maria~bionic

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity`
--

DROP TABLE IF EXISTS `activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class` varchar(100) NOT NULL,
  `module` varchar(100) DEFAULT '',
  `object_model` varchar(100) DEFAULT '',
  `object_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity`
--

LOCK TABLES `activity` WRITE;
/*!40000 ALTER TABLE `activity` DISABLE KEYS */;
/*!40000 ALTER TABLE `activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text DEFAULT NULL,
  `object_model` varchar(100) NOT NULL,
  `object_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comment-created_by` (`created_by`),
  CONSTRAINT `fk_comment-created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content`
--

DROP TABLE IF EXISTS `content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` varchar(45) NOT NULL,
  `object_model` varchar(100) NOT NULL,
  `object_id` int(11) NOT NULL,
  `visibility` tinyint(4) DEFAULT NULL,
  `pinned` tinyint(4) DEFAULT NULL,
  `archived` tinytext DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `contentcontainer_id` int(11) DEFAULT NULL,
  `stream_sort_date` datetime DEFAULT NULL,
  `stream_channel` char(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_object_model` (`object_model`,`object_id`),
  UNIQUE KEY `index_guid` (`guid`),
  KEY `fk-contentcontainer` (`contentcontainer_id`),
  KEY `fk-create-user` (`created_by`),
  KEY `fk-update-user` (`updated_by`),
  KEY `stream_channe` (`stream_channel`),
  CONSTRAINT `fk-contentcontainer` FOREIGN KEY (`contentcontainer_id`) REFERENCES `contentcontainer` (`id`) ON DELETE SET NULL,
  CONSTRAINT `fk-create-user` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `fk-update-user` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content`
--

LOCK TABLES `content` WRITE;
/*!40000 ALTER TABLE `content` DISABLE KEYS */;
/*!40000 ALTER TABLE `content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_tag`
--

DROP TABLE IF EXISTS `content_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `module_id` varchar(100) NOT NULL,
  `contentcontainer_id` int(11) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `color` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx-content-tag` (`module_id`,`contentcontainer_id`,`name`),
  KEY `fk-content-tag-container-id` (`contentcontainer_id`),
  KEY `fk-content-tag-parent-id` (`parent_id`),
  CONSTRAINT `fk-content-tag-container-id` FOREIGN KEY (`contentcontainer_id`) REFERENCES `contentcontainer` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk-content-tag-parent-id` FOREIGN KEY (`parent_id`) REFERENCES `content_tag` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_tag`
--

LOCK TABLES `content_tag` WRITE;
/*!40000 ALTER TABLE `content_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `content_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_tag_relation`
--

DROP TABLE IF EXISTS `content_tag_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_tag_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-content-tag-rel-content-id` (`content_id`),
  KEY `fk-content-tag-rel-tag-id` (`tag_id`),
  CONSTRAINT `fk-content-tag-rel-content-id` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk-content-tag-rel-tag-id` FOREIGN KEY (`tag_id`) REFERENCES `content_tag` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_tag_relation`
--

LOCK TABLES `content_tag_relation` WRITE;
/*!40000 ALTER TABLE `content_tag_relation` DISABLE KEYS */;
/*!40000 ALTER TABLE `content_tag_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contentcontainer`
--

DROP TABLE IF EXISTS `contentcontainer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contentcontainer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` char(36) NOT NULL,
  `class` char(60) NOT NULL,
  `pk` int(11) DEFAULT NULL,
  `owner_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_guid` (`guid`),
  UNIQUE KEY `unique_target` (`class`,`pk`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contentcontainer`
--

LOCK TABLES `contentcontainer` WRITE;
/*!40000 ALTER TABLE `contentcontainer` DISABLE KEYS */;
INSERT INTO `contentcontainer` VALUES (1,'d0b68d72-03a2-4bd1-98dc-3e5087662589','humhub\\modules\\user\\models\\User',1,1);
/*!40000 ALTER TABLE `contentcontainer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contentcontainer_module`
--

DROP TABLE IF EXISTS `contentcontainer_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contentcontainer_module` (
  `contentcontainer_id` int(11) NOT NULL,
  `module_id` char(100) NOT NULL,
  `module_state` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`contentcontainer_id`,`module_id`),
  CONSTRAINT `fk_contentcontainer` FOREIGN KEY (`contentcontainer_id`) REFERENCES `contentcontainer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contentcontainer_module`
--

LOCK TABLES `contentcontainer_module` WRITE;
/*!40000 ALTER TABLE `contentcontainer_module` DISABLE KEYS */;
/*!40000 ALTER TABLE `contentcontainer_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contentcontainer_permission`
--

DROP TABLE IF EXISTS `contentcontainer_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contentcontainer_permission` (
  `permission_id` varchar(150) NOT NULL,
  `contentcontainer_id` int(11) NOT NULL,
  `group_id` varchar(50) NOT NULL,
  `module_id` varchar(50) NOT NULL,
  `class` varchar(255) DEFAULT NULL,
  `state` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`permission_id`,`group_id`,`module_id`,`contentcontainer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contentcontainer_permission`
--

LOCK TABLES `contentcontainer_permission` WRITE;
/*!40000 ALTER TABLE `contentcontainer_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `contentcontainer_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contentcontainer_setting`
--

DROP TABLE IF EXISTS `contentcontainer_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contentcontainer_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` varchar(50) NOT NULL,
  `contentcontainer_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings-unique` (`module_id`,`contentcontainer_id`,`name`),
  KEY `fk-contentcontainerx` (`contentcontainer_id`),
  CONSTRAINT `fk-contentcontainerx` FOREIGN KEY (`contentcontainer_id`) REFERENCES `contentcontainer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contentcontainer_setting`
--

LOCK TABLES `contentcontainer_setting` WRITE;
/*!40000 ALTER TABLE `contentcontainer_setting` DISABLE KEYS */;
INSERT INTO `contentcontainer_setting` VALUES (1,'tour',1,'welcome','1');
/*!40000 ALTER TABLE `contentcontainer_setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file`
--

DROP TABLE IF EXISTS `file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` varchar(45) DEFAULT NULL,
  `object_model` varchar(100) DEFAULT '',
  `object_id` varchar(100) DEFAULT '',
  `file_name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `mime_type` varchar(150) DEFAULT NULL,
  `size` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `show_in_stream` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `index_object` (`object_model`,`object_id`),
  KEY `fk_file-created_by` (`created_by`),
  CONSTRAINT `fk_file-created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file`
--

LOCK TABLES `file` WRITE;
/*!40000 ALTER TABLE `file` DISABLE KEYS */;
/*!40000 ALTER TABLE `file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group`
--

DROP TABLE IF EXISTS `group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `space_id` int(10) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `ldap_dn` varchar(255) DEFAULT NULL,
  `is_admin_group` tinyint(1) NOT NULL DEFAULT 0,
  `show_at_registration` tinyint(1) NOT NULL DEFAULT 1,
  `show_at_directory` tinyint(1) NOT NULL DEFAULT 1,
  `sort_order` int(11) NOT NULL DEFAULT 100,
  PRIMARY KEY (`id`),
  KEY `fk_group-space_id` (`space_id`),
  CONSTRAINT `fk_group-space_id` FOREIGN KEY (`space_id`) REFERENCES `space` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group`
--

LOCK TABLES `group` WRITE;
/*!40000 ALTER TABLE `group` DISABLE KEYS */;
INSERT INTO `group` VALUES (1,NULL,'Administrator','Administrator Group','2020-06-09 11:50:19',NULL,NULL,NULL,NULL,1,0,0,100),(2,NULL,'Users','Example Group by Installer','2020-06-09 11:50:35',NULL,'2020-06-09 11:50:35',NULL,NULL,0,1,0,100);
/*!40000 ALTER TABLE `group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_permission`
--

DROP TABLE IF EXISTS `group_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_permission` (
  `permission_id` varchar(150) NOT NULL,
  `group_id` int(11) NOT NULL,
  `module_id` varchar(50) NOT NULL,
  `class` varchar(255) DEFAULT NULL,
  `state` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`permission_id`,`group_id`,`module_id`),
  KEY `fk_group_permission-group_id` (`group_id`),
  CONSTRAINT `fk_group_permission-group_id` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_permission`
--

LOCK TABLES `group_permission` WRITE;
/*!40000 ALTER TABLE `group_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_user`
--

DROP TABLE IF EXISTS `group_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `is_group_manager` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx-group_user` (`user_id`,`group_id`),
  KEY `fk-group-group` (`group_id`),
  CONSTRAINT `fk-group-group` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk-user-group` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_user`
--

LOCK TABLES `group_user` WRITE;
/*!40000 ALTER TABLE `group_user` DISABLE KEYS */;
INSERT INTO `group_user` VALUES (1,1,1,0,'2020-06-09 11:50:36',NULL,'2020-06-09 11:50:36',NULL);
/*!40000 ALTER TABLE `group_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `like`
--

DROP TABLE IF EXISTS `like`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `like` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `target_user_id` int(11) DEFAULT NULL,
  `object_model` varchar(100) NOT NULL,
  `object_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_object` (`object_model`,`object_id`),
  KEY `fk_like-created_by` (`created_by`),
  KEY `fk_like-target_user_id` (`target_user_id`),
  CONSTRAINT `fk_like-created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_like-target_user_id` FOREIGN KEY (`target_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `like`
--

LOCK TABLES `like` WRITE;
/*!40000 ALTER TABLE `like` DISABLE KEYS */;
/*!40000 ALTER TABLE `like` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `live`
--

DROP TABLE IF EXISTS `live`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `live` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contentcontainer_id` int(11) DEFAULT NULL,
  `visibility` int(1) DEFAULT NULL,
  `serialized_data` text NOT NULL,
  `created_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `contentcontainer` (`contentcontainer_id`),
  CONSTRAINT `contentcontainer` FOREIGN KEY (`contentcontainer_id`) REFERENCES `contentcontainer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `live`
--

LOCK TABLES `live` WRITE;
/*!40000 ALTER TABLE `live` DISABLE KEYS */;
/*!40000 ALTER TABLE `live` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `level` int(11) DEFAULT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `log_time` double DEFAULT NULL,
  `prefix` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_log_level` (`level`),
  KEY `idx_log_category` (`category`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log`
--

LOCK TABLES `log` WRITE;
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
INSERT INTO `log` VALUES (1,1,'yii\\base\\ErrorException:2',1591696553.5718,'[86.211.47.45][1][3fe86e9tgbteeetii7a4a0d3nj]','yii\\base\\ErrorException: file_put_contents(/var/www/localhost/htdocs/protected/config/dynamic.php): failed to open stream: Permission denied in /var/www/localhost/htdocs/protected/humhub/libs/DynamicConfig.php:71\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleError(2, \'file_put_conten...\', \'/var/www/localh...\', 71, Array)\n#1 /var/www/localhost/htdocs/protected/humhub/libs/DynamicConfig.php(71): file_put_contents(\'/var/www/localh...\', \'<?php return ar...\')\n#2 /var/www/localhost/htdocs/protected/humhub/libs/DynamicConfig.php(180): humhub\\libs\\DynamicConfig::save(Array)\n#3 /var/www/localhost/htdocs/protected/humhub/modules/admin/models/forms/BasicSettingsForm.php(143): humhub\\libs\\DynamicConfig::rewrite()\n#4 /var/www/localhost/htdocs/protected/humhub/modules/admin/controllers/SettingController.php(77): humhub\\modules\\admin\\models\\forms\\BasicSettingsForm->save()\n#5 [internal function]: humhub\\modules\\admin\\controllers\\SettingController->actionBasic()\n#6 /var/www/localhost/htdocs/protected/vendor/yiisoft/yii2/base/InlineAction.php(57): call_user_func_array(Array, Array)\n#7 /var/www/localhost/htdocs/protected/vendor/yiisoft/yii2/base/Controller.php(157): yii\\base\\InlineAction->runWithParams(Array)\n#8 /var/www/localhost/htdocs/protected/vendor/yiisoft/yii2/base/Module.php(528): yii\\base\\Controller->runAction(\'basic\', Array)\n#9 /var/www/localhost/htdocs/protected/vendor/yiisoft/yii2/web/Application.php(103): yii\\base\\Module->runAction(\'admin/setting/b...\', Array)\n#10 /var/www/localhost/htdocs/protected/vendor/yiisoft/yii2/base/Application.php(386): yii\\web\\Application->handleRequest(Object(humhub\\components\\Request))\n#11 /var/www/localhost/htdocs/index.php(25): yii\\base\\Application->run()\n#12 {main}'),(2,4,'application',1591696553.3374,'[86.211.47.45][1][3fe86e9tgbteeetii7a4a0d3nj]','$_GET = [\n    \'_pjax\' => \'#layout-content\'\n    \'_\' => \'1591696526266\'\n]\n\n$_SERVER = [\n    \'USER\' => \'nginx\'\n    \'HOME\' => \'/var/lib/nginx\'\n    \'HTTP_COOKIE\' => \'_ga=GA1.2.1109100596.1590352922; portainer.datatable_text_filter_home_endpoints=; portainer.datatable_settings_containers=%7B%22open%22%3Afalse%2C%22repeater%22%3A%7B%22autoRefresh%22%3Afalse%2C%22refreshRate%22%3A%2230%22%7D%2C%22truncateContainerName%22%3Atrue%2C%22containerNameTruncateSize%22%3A32%2C%22showQuickActionStats%22%3Atrue%2C%22showQuickActionLogs%22%3Atrue%2C%22showQuickActionExec%22%3Atrue%2C%22showQuickActionInspect%22%3Atrue%2C%22showQuickActionAttach%22%3Afalse%7D; portainer.datatable_settings_stacks=%7B%22open%22%3Afalse%2C%22repeater%22%3A%7B%22autoRefresh%22%3Afalse%2C%22refreshRate%22%3A%2230%22%7D%7D; portainer.datatable_settings_stack-containers=%7B%22open%22%3Afalse%2C%22repeater%22%3A%7B%22autoRefresh%22%3Afalse%2C%22refreshRate%22%3A%2230%22%7D%2C%22truncateContainerName%22%3Atrue%2C%22containerNameTruncateSize%22%3A32%2C%22showQuickActionStats%22%3Atrue%2C%22showQuickActionLogs%22%3Atrue%2C%22showQuickActionExec%22%3Atrue%2C%22showQuickActionInspect%22%3Atrue%2C%22showQuickActionAttach%22%3Afalse%7D; portainer.datatable_settings_images=%7B%22open%22%3Afalse%2C%22repeater%22%3A%7B%22autoRefresh%22%3Afalse%2C%22refreshRate%22%3A%2230%22%7D%7D; portainer.datatable_settings_container-networks=%7B%22open%22%3Afalse%2C%22repeater%22%3A%7B%22autoRefresh%22%3Afalse%2C%22refreshRate%22%3A%2230%22%7D%7D; portainer.datatable_settings_networks=%7B%22open%22%3Afalse%2C%22repeater%22%3A%7B%22autoRefresh%22%3Afalse%2C%22refreshRate%22%3A%2230%22%7D%7D; portainer.datatable_settings_volumes=%7B%22open%22%3Afalse%2C%22repeater%22%3A%7B%22autoRefresh%22%3Afalse%2C%22refreshRate%22%3A%2230%22%7D%7D; portainer.datatable_settings_events=%7B%22open%22%3Afalse%2C%22repeater%22%3A%7B%22autoRefresh%22%3Afalse%2C%22refreshRate%22%3A%2230%22%7D%7D; portainer.datatable_settings_users=%7B%22open%22%3Afalse%2C%22repeater%22%3A%7B%22autoRefresh%22%3Afalse%2C%22refreshRate%22%3A%2230%22%7D%7D; pma_lang=fr; portainer.datatable_settings_container-processes=%7B%22open%22%3Afalse%2C%22repeater%22%3A%7B%22autoRefresh%22%3Afalse%2C%22refreshRate%22%3A%2230%22%7D%7D; PrestaShop-88e085f7e13b97b5a71e7107ef750eab=def50200de37cb1b45cd922b8e575eb9732082dc9d7351e4abef831815386f314bd4885d9cbd22e9acc903dfb0d6d786a748f01f7a7d9e211ac0af880d05131761d386f7ac7cdce43d85f39ac3962101342d84b907b97695ed8ff5b552022069996d2869471acb84536614ff98665cedb73ba36140fc80b08307dab5a435895ec3833bd6309dfdd05fe24d1412c9bdd4a58fd3f709864e68ec273b1f0cd2d062c296465f3e1152c4dc62b79de5a323198a59fdba0bd06375ee50fbf0bc47d4ebf9efa52b4348e1bea2c969a339c4cab809b75d09f6a96bffaa; wp-settings-time-3=1590502480; portainer.datatable_settings_roles=%7B%22open%22%3Afalse%2C%22repeater%22%3A%7B%22autoRefresh%22%3Afalse%2C%22refreshRate%22%3A%2230%22%7D%7D; portainer.datatable_settings_registries=%7B%22open%22%3Afalse%2C%22repeater%22%3A%7B%22autoRefresh%22%3Afalse%2C%22refreshRate%22%3A%2230%22%7D%7D; session_id=d7c487e6d3a4ba3338d1bd6420f01f6810d96aa7; PrestaShop-0ec6c27e799ab2011b4b370e1a1b2eb3=def502006e5d5338e5e57fe340059f99358d34a2aa050484b2f042de7e732d2f95014013f22aec5bc325674764d57692195d0e90026846a657c3cd4014c33175a17f10e35b9af8d2d8188e3c77957126c1ab46667f8c8631f5395b2fb436667f61e01039743ded6318530ceb99cf79e83825101e822e418bff36866c718dfb307a432f12269ecdee94f066e91c8dcaaf08cc2523505db97a669f974e1b5ecb50da54cd; optimizelyEndUserId=oeu1591378343706r0.5721375720505779; wordpress_test_cookie=WP+Cookie+check; tk_ai=woo%3AUy9O%2B%2BK3NNr%2BIEcrpGxH4XXx; wordpress_logged_in_d1db9fe70df6b52bef87858d8f7b7ceb=adminWC%7C1591786580%7C278LfPoZdDgPoRjV2ITmqlEUlNb69XDi9RpuUdVLNud%7C6deb7f01f8b08513bfdfa0fd76e0eb0dc7a717f8472c4e12b708d16ece6a41e0; wp-settings-time-4=1591613816; PrestaShop-d5f8ea8b6c397a0e1acec6e5bde709b1=def502000969e7aba34da9a32b8a4890e0eb49b88922e6a22352eea041181da17aef9868cf3161a3dc30cb030989df710eedd1b9c88a1044eed10fdd8f4eda3ab133f9c045dc59dd3f14197eddbf8a1d0cd18099d32fd127d1ab9704236ef8c57250b095d3f9e5741141e85101b3457c41376713b16b8c39e8493b8de27ad88b3ed091ce0b02521b96dfc353f1c7aa16daf8c0c23dd83c9fc138b93e7e13f92e3a2855c3dd705d096abf4b91af083211d8407706ab1fcf46a3aeccccd8612b47016bca4956569177e3276bed0639311771e2d29bc88b7fba3543fdc864be7fd94bb7b9618c577bf663693ab9245227804f5b2822adcd20dbb4dde18e59a7c10f5a04a0abad8f20ff4a12a4cc25652850df9d0e1e1482b6dc700c4918ce1bc37374645faea017ad3c5a303890; PrestaShop-a8c2e2f9d846a8c5ca01f11e2068efcd=def50200e2cd6dca1d4e7794c524e5f005a0241cb841a2c57e6ac65bb872f39cdb89403dc6362bdfd386d173be56da9ec2ea467a65f8a449622dca853c21fb09e6c1f9f847ab4d5ab3f526d5021716848cc451cadda5a5131ed52c7b07de0efca11fda36c503ffb184c5e1336c295cf35f9a912ae339a73da46c3bff038dc6feabfa0999ad7b2a4513f096a17ff40dd1369ca008c93b7c8560308ddac71109b966a55adc24860f284a7f92496299dfbda81849b2d19586f5393fc1ddd8c565aca5833a9569ad3220115d60ef1d4c6a10c47fef47781b40be433781d4f895be72f5b80a3646ad7813afd1a1d823bb15024f05e8aa376d303bdb6eee3b464ca490fba58b18a66b8806779ac7184c638b1c8393b4a298747b8ee0f2d17b8c253fb9afc93ac5b09a56fb4a82ffc0b929db71dde68e03e7c8139d9c4915285c89a1b28666df588266b64d15517d917de3e1701161facc8b1928f20002ef; pm_getting-started-panel=expanded; pm_panel-activities=expanded; sugar_user_theme=SuiteP; EmailGridWidths=0=10&1=10&2=150&3=250&4=175&5=125; PHPSESSID=3fe86e9tgbteeetii7a4a0d3nj; _identity=214df8cba92db0cf34d7d037d415e02a6272b43ec777e8e4f229601be4031f93a%3A2%3A%7Bi%3A0%3Bs%3A9%3A%22_identity%22%3Bi%3A1%3Bs%3A50%3A%22%5B1%2C%22d0b68d72-03a2-4bd1-98dc-3e5087662589%22%2C2592000%5D%22%3B%7D; _csrf=b2b0e86d8b58afb700bf8c7f6c4457f8ea13e87c70082cde31490b4cb900f030a%3A2%3A%7Bi%3A0%3Bs%3A5%3A%22_csrf%22%3Bi%3A1%3Bs%3A32%3A%220dwYlIIgCieeCryws5RVSi1YSTFKKiZi%22%3B%7D\'\n    \'HTTP_ACCEPT_LANGUAGE\' => \'fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7\'\n    \'HTTP_ACCEPT_ENCODING\' => \'gzip, deflate\'\n    \'HTTP_REFERER\' => \'http://llb.ac-corse.fr:10242/admin/setting/basic\'\n    \'HTTP_ACCEPT\' => \'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9\'\n    \'HTTP_USER_AGENT\' => \'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36\'\n    \'HTTP_CONTENT_TYPE\' => \'application/x-www-form-urlencoded\'\n    \'HTTP_ORIGIN\' => \'http://llb.ac-corse.fr:10242\'\n    \'HTTP_UPGRADE_INSECURE_REQUESTS\' => \'1\'\n    \'HTTP_CACHE_CONTROL\' => \'max-age=0\'\n    \'HTTP_CONTENT_LENGTH\' => \'543\'\n    \'HTTP_CONNECTION\' => \'keep-alive\'\n    \'HTTP_HOST\' => \'llb.ac-corse.fr:10242\'\n    \'SCRIPT_FILENAME\' => \'/var/www/localhost/htdocs/index.php\'\n    \'PATH_INFO\' => \'\'\n    \'REDIRECT_STATUS\' => \'200\'\n    \'SERVER_NAME\' => \'_\'\n    \'SERVER_PORT\' => \'80\'\n    \'SERVER_ADDR\' => \'192.168.97.29\'\n    \'REMOTE_PORT\' => \'51518\'\n    \'REMOTE_ADDR\' => \'86.211.47.45\'\n    \'SERVER_SOFTWARE\' => \'nginx/1.14.2\'\n    \'GATEWAY_INTERFACE\' => \'CGI/1.1\'\n    \'REQUEST_SCHEME\' => \'http\'\n    \'SERVER_PROTOCOL\' => \'HTTP/1.1\'\n    \'DOCUMENT_ROOT\' => \'/var/www/localhost/htdocs\'\n    \'DOCUMENT_URI\' => \'/index.php\'\n    \'REQUEST_URI\' => \'/admin/setting/basic?_pjax=%23layout-content&_=1591696526266\'\n    \'SCRIPT_NAME\' => \'/index.php\'\n    \'CONTENT_LENGTH\' => \'543\'\n    \'CONTENT_TYPE\' => \'application/x-www-form-urlencoded\'\n    \'REQUEST_METHOD\' => \'POST\'\n    \'QUERY_STRING\' => \'_pjax=%23layout-content&_=1591696526266\'\n    \'FCGI_ROLE\' => \'RESPONDER\'\n    \'PHP_SELF\' => \'/index.php\'\n    \'REQUEST_TIME_FLOAT\' => 1591696553.3259\n    \'REQUEST_TIME\' => 1591696553\n]'),(3,2,'application',1591951974.6012,'[86.211.47.45][1][-]','Deleted inconsistent notification with id 1. Call to an inconsistent polymorphic relation detected on humhub\\modules\\notification\\models\\Notification (:)'),(4,4,'application',1591951974.3977,'[86.211.47.45][1][-]','$_GET = [\n    \'from\' => \'0\'\n]\n\n$_SERVER = [\n    \'USER\' => \'nginx\'\n    \'HOME\' => \'/var/lib/nginx\'\n    \'HTTP_COOKIE\' => \'_ga=GA1.2.1109100596.1590352922; portainer.datatable_text_filter_home_endpoints=; portainer.datatable_settings_containers=%7B%22open%22%3Afalse%2C%22repeater%22%3A%7B%22autoRefresh%22%3Afalse%2C%22refreshRate%22%3A%2230%22%7D%2C%22truncateContainerName%22%3Atrue%2C%22containerNameTruncateSize%22%3A32%2C%22showQuickActionStats%22%3Atrue%2C%22showQuickActionLogs%22%3Atrue%2C%22showQuickActionExec%22%3Atrue%2C%22showQuickActionInspect%22%3Atrue%2C%22showQuickActionAttach%22%3Afalse%7D; portainer.datatable_settings_stacks=%7B%22open%22%3Afalse%2C%22repeater%22%3A%7B%22autoRefresh%22%3Afalse%2C%22refreshRate%22%3A%2230%22%7D%7D; portainer.datatable_settings_stack-containers=%7B%22open%22%3Afalse%2C%22repeater%22%3A%7B%22autoRefresh%22%3Afalse%2C%22refreshRate%22%3A%2230%22%7D%2C%22truncateContainerName%22%3Atrue%2C%22containerNameTruncateSize%22%3A32%2C%22showQuickActionStats%22%3Atrue%2C%22showQuickActionLogs%22%3Atrue%2C%22showQuickActionExec%22%3Atrue%2C%22showQuickActionInspect%22%3Atrue%2C%22showQuickActionAttach%22%3Afalse%7D; portainer.datatable_settings_images=%7B%22open%22%3Afalse%2C%22repeater%22%3A%7B%22autoRefresh%22%3Afalse%2C%22refreshRate%22%3A%2230%22%7D%7D; portainer.datatable_settings_container-networks=%7B%22open%22%3Afalse%2C%22repeater%22%3A%7B%22autoRefresh%22%3Afalse%2C%22refreshRate%22%3A%2230%22%7D%7D; portainer.datatable_settings_networks=%7B%22open%22%3Afalse%2C%22repeater%22%3A%7B%22autoRefresh%22%3Afalse%2C%22refreshRate%22%3A%2230%22%7D%7D; portainer.datatable_settings_volumes=%7B%22open%22%3Afalse%2C%22repeater%22%3A%7B%22autoRefresh%22%3Afalse%2C%22refreshRate%22%3A%2230%22%7D%7D; portainer.datatable_settings_events=%7B%22open%22%3Afalse%2C%22repeater%22%3A%7B%22autoRefresh%22%3Afalse%2C%22refreshRate%22%3A%2230%22%7D%7D; portainer.datatable_settings_users=%7B%22open%22%3Afalse%2C%22repeater%22%3A%7B%22autoRefresh%22%3Afalse%2C%22refreshRate%22%3A%2230%22%7D%7D; pma_lang=fr; portainer.datatable_settings_container-processes=%7B%22open%22%3Afalse%2C%22repeater%22%3A%7B%22autoRefresh%22%3Afalse%2C%22refreshRate%22%3A%2230%22%7D%7D; PrestaShop-88e085f7e13b97b5a71e7107ef750eab=def50200de37cb1b45cd922b8e575eb9732082dc9d7351e4abef831815386f314bd4885d9cbd22e9acc903dfb0d6d786a748f01f7a7d9e211ac0af880d05131761d386f7ac7cdce43d85f39ac3962101342d84b907b97695ed8ff5b552022069996d2869471acb84536614ff98665cedb73ba36140fc80b08307dab5a435895ec3833bd6309dfdd05fe24d1412c9bdd4a58fd3f709864e68ec273b1f0cd2d062c296465f3e1152c4dc62b79de5a323198a59fdba0bd06375ee50fbf0bc47d4ebf9efa52b4348e1bea2c969a339c4cab809b75d09f6a96bffaa; wp-settings-time-3=1590502480; portainer.datatable_settings_roles=%7B%22open%22%3Afalse%2C%22repeater%22%3A%7B%22autoRefresh%22%3Afalse%2C%22refreshRate%22%3A%2230%22%7D%7D; portainer.datatable_settings_registries=%7B%22open%22%3Afalse%2C%22repeater%22%3A%7B%22autoRefresh%22%3Afalse%2C%22refreshRate%22%3A%2230%22%7D%7D; session_id=d7c487e6d3a4ba3338d1bd6420f01f6810d96aa7; PrestaShop-0ec6c27e799ab2011b4b370e1a1b2eb3=def502006e5d5338e5e57fe340059f99358d34a2aa050484b2f042de7e732d2f95014013f22aec5bc325674764d57692195d0e90026846a657c3cd4014c33175a17f10e35b9af8d2d8188e3c77957126c1ab46667f8c8631f5395b2fb436667f61e01039743ded6318530ceb99cf79e83825101e822e418bff36866c718dfb307a432f12269ecdee94f066e91c8dcaaf08cc2523505db97a669f974e1b5ecb50da54cd; optimizelyEndUserId=oeu1591378343706r0.5721375720505779; wp-settings-time-4=1591613816; sugar_user_theme=SuiteP; EmailGridWidths=0=10&1=10&2=150&3=250&4=175&5=125; _identity=214df8cba92db0cf34d7d037d415e02a6272b43ec777e8e4f229601be4031f93a%3A2%3A%7Bi%3A0%3Bs%3A9%3A%22_identity%22%3Bi%3A1%3Bs%3A50%3A%22%5B1%2C%22d0b68d72-03a2-4bd1-98dc-3e5087662589%22%2C2592000%5D%22%3B%7D; PrestaShop-d5f8ea8b6c397a0e1acec6e5bde709b1=def50200522f2d6256677988286ef395cb4629b41ad0cc00c156918258a5d98208202b95bb4e3d39554d17d2972bfda1fecddf8b9c946de70c4c6b25d3e36df04875e9682b6a472e021c1428b7cc4f76d0847558a19745a6d6253a45f90338e4fda151115274761770199b6c9ac4a3d9edbd2c0383f1faf75c71aedd3e3a6c484556b7a9d15b8afc578e57148fa33e218a915de1dd1555a6cfb631d7fef94688ca5b9f5717b4f644263a3310051f09dc500991aaa62d7fcb371c403374824102; PrestaShop-a8c2e2f9d846a8c5ca01f11e2068efcd=def502004138f05fc2ce65e1f48161e6a578b377ab4f12d052e9b5003bad48fe28db1c1c3dae4be7ed0968acf8bb20f417c1c00b57c185e813030755bfa7efa20c03220f7446589f823359f2733aee4813c74b9f8bc338a29521952a3daec7fc31b42e1f01869b6925cb4e4930c868db11bd785cb67e3d0b1fab54bb7e41943a818c9a1364eee7ef50967f35a32d5ec68a0e5b7ca32ebec640beb543b9880bead5e540730142f863230b279bf5ec8e89c81f34b58f36e1eee3fd92cceb8435244a0316776546732c7af37918e85080bfc4355840e83bd733aa60ea5990efb8ae12daefdf074b4f3c676602f4ff887dfe889813e018986589d6adee304bb9c7ed694b36c3fb47c6a535c197b6ee5509eb7baf56c941cadd3fdb233a9f79ac9030abeb3f5e4374697ea82cff599aa207dd6fb9d98ade383f77bf4e7f0ad39d6d49a028cbe23154441502df5da6c9ebc96d1f3023d586d6219ddafc46ba; _gid=GA1.2.1689827430.1591951080; portainer.datatable_text_filter_containers=nil; PHPSESSID=g7mi27pl3es5cocqaab4djl477; _csrf=6ad41f2e8d42c420f2975b94c00d3d52ad7fd7a4da7fff0d78c6cd6cc15d9d14a%3A2%3A%7Bi%3A0%3Bs%3A5%3A%22_csrf%22%3Bi%3A1%3Bs%3A32%3A%2215Ti-eCDHXc8kadv5Fuc8bGspsx_0Fji%22%3B%7D; pm_getting-started-panel=expanded; pm_panel-activities=expanded\'\n    \'HTTP_ACCEPT_LANGUAGE\' => \'fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7\'\n    \'HTTP_ACCEPT_ENCODING\' => \'gzip, deflate\'\n    \'HTTP_REFERER\' => \'http://llb.ac-corse.fr:10240/dashboard\'\n    \'HTTP_X_REQUESTED_WITH\' => \'XMLHttpRequest\'\n    \'HTTP_USER_AGENT\' => \'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36\'\n    \'HTTP_X_CSRF_TOKEN\' => \'djeUsHn2yb25k0X-mS8xyUYMuplEEACzMyCPnPFdF5lHAsDZVJOK-fHLJsbyTlW_c0rP-nxyR8BDU_fDwRt98A==\'\n    \'HTTP_ACCEPT\' => \'*/*\'\n    \'HTTP_CONNECTION\' => \'keep-alive\'\n    \'HTTP_HOST\' => \'llb.ac-corse.fr:10240\'\n    \'SCRIPT_FILENAME\' => \'/var/www/localhost/htdocs/index.php\'\n    \'PATH_INFO\' => \'\'\n    \'REDIRECT_STATUS\' => \'200\'\n    \'SERVER_NAME\' => \'_\'\n    \'SERVER_PORT\' => \'80\'\n    \'SERVER_ADDR\' => \'192.168.97.19\'\n    \'REMOTE_PORT\' => \'57043\'\n    \'REMOTE_ADDR\' => \'86.211.47.45\'\n    \'SERVER_SOFTWARE\' => \'nginx/1.14.2\'\n    \'GATEWAY_INTERFACE\' => \'CGI/1.1\'\n    \'REQUEST_SCHEME\' => \'http\'\n    \'SERVER_PROTOCOL\' => \'HTTP/1.1\'\n    \'DOCUMENT_ROOT\' => \'/var/www/localhost/htdocs\'\n    \'DOCUMENT_URI\' => \'/index.php\'\n    \'REQUEST_URI\' => \'/notification/list?from=0\'\n    \'SCRIPT_NAME\' => \'/index.php\'\n    \'CONTENT_LENGTH\' => \'\'\n    \'CONTENT_TYPE\' => \'\'\n    \'REQUEST_METHOD\' => \'GET\'\n    \'QUERY_STRING\' => \'from=0\'\n    \'FCGI_ROLE\' => \'RESPONDER\'\n    \'PHP_SELF\' => \'/index.php\'\n    \'REQUEST_TIME_FLOAT\' => 1591951974.3864\n    \'REQUEST_TIME\' => 1591951974\n]');
/*!40000 ALTER TABLE `log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logging`
--

DROP TABLE IF EXISTS `logging`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logging` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` varchar(128) DEFAULT NULL,
  `category` varchar(128) DEFAULT NULL,
  `logtime` int(11) DEFAULT NULL,
  `message` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logging`
--

LOCK TABLES `logging` WRITE;
/*!40000 ALTER TABLE `logging` DISABLE KEYS */;
/*!40000 ALTER TABLE `logging` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1591696182),('m131023_164513_initial',1591696185),('m131023_165411_initial',1591696185),('m131023_165625_initial',1591696186),('m131023_165755_initial',1591696186),('m131023_165835_initial',1591696186),('m131023_170033_initial',1591696186),('m131023_170135_initial',1591696186),('m131023_170159_initial',1591696187),('m131023_170253_initial',1591696187),('m131023_170339_initial',1591696187),('m131203_110444_oembed',1591696187),('m131213_165552_user_optimize',1591696188),('m140226_111945_ldap',1591696188),('m140303_125031_password',1591696188),('m140304_142711_memberautoadd',1591696188),('m140321_000917_content',1591696189),('m140324_170617_membership',1591696189),('m140507_150421_create_settings_table',1591696189),('m140507_171527_create_settings_table',1591696189),('m140512_141414_i18n_profilefields',1591696189),('m140513_180317_createlogging',1591696190),('m140701_000611_profile_genderfield',1591696190),('m140701_074404_protect_default_profilefields',1591696190),('m140702_143912_notify_notification_unify',1591696190),('m140703_104527_profile_birthdayfield',1591696190),('m140704_080659_installationid',1591696190),('m140705_065525_emailing_settings',1591696191),('m140706_135210_lastlogin',1591696191),('m140829_122906_delete',1591696191),('m140830_145504_following',1591696192),('m140901_080147_indizies',1591696192),('m140901_080432_indices',1591696192),('m140901_112246_addState',1591696193),('m140901_153403_addState',1591696194),('m140901_170329_group_create_space',1591696194),('m140902_091234_session_key_length',1591696194),('m140907_140822_zip_field_to_text',1591696194),('m140930_205511_fix_default',1591696195),('m140930_205859_fix_default',1591696195),('m140930_210142_fix_default',1591696196),('m140930_210635_fix_default',1591696196),('m140930_212528_fix_default',1591696197),('m141015_173305_follow_notifications',1591696197),('m141019_093319_mentioning',1591696197),('m141020_162639_fix_default',1591696198),('m141020_193920_rm_alsocreated',1591696198),('m141020_193931_rm_alsoliked',1591696198),('m141021_162639_oembed_setting',1591696198),('m141022_094635_addDefaults',1591696198),('m141106_185632_log_init',1591696199),('m150204_103433_html5_notified',1591696199),('m150210_190006_user_invite_lang',1591696199),('m150302_114347_add_visibility',1591696199),('m150322_194403_remove_type_field',1591696199),('m150322_195619_allowedExt2Text',1591696199),('m150429_223856_optimize',1591696199),('m150510_102900_update',1591696200),('m150629_220311_change',1591696200),('m150703_012735_typelength',1591696200),('m150703_024635_activityTypes',1591696200),('m150703_033650_namespace',1591696200),('m150703_130157_migrate',1591696200),('m150704_005338_namespace',1591696200),('m150704_005418_namespace',1591696200),('m150704_005434_namespace',1591696200),('m150704_005452_namespace',1591696200),('m150704_005504_namespace',1591696200),('m150713_054441_timezone',1591696200),('m150714_093525_activity',1591696201),('m150714_100355_cleanup',1591696203),('m150831_061628_notifications',1591696203),('m150910_223305_fix_user_follow',1591696203),('m150924_133344_update_notification_fix',1591696203),('m150924_154635_user_invite_add_first_lastname',1591696203),('m150927_190830_create_contentcontainer',1591696204),('m150928_103711_permissions',1591696205),('m150928_134934_groups',1591696205),('m150928_140718_setColorVariables',1591696205),('m151010_124437_group_permissions',1591696206),('m151010_175000_default_visibility',1591696206),('m151013_223814_include_dashboard',1591696206),('m151022_131128_module_fix',1591696206),('m151106_090948_addColor',1591696206),('m151223_171310_fix_notifications',1591696206),('m151226_164234_authclient',1591696207),('m160125_053702_stored_filename',1591696207),('m160205_203840_foreign_keys',1591696211),('m160205_203913_foreign_keys',1591696212),('m160205_203939_foreign_keys',1591696213),('m160205_203955_foreign_keys',1591696214),('m160205_204000_foreign_keys',1591696214),('m160205_204010_foreign_keys',1591696214),('m160205_205540_foreign_keys',1591696215),('m160216_160119_initial',1591696216),('m160217_161220_addCanLeaveFlag',1591696216),('m160220_013525_contentcontainer_id',1591696218),('m160221_222312_public_permission_change',1591696218),('m160225_180229_remove_website',1591696218),('m160227_073020_birthday_date',1591696218),('m160229_162959_multiusergroups',1591696220),('m160309_141222_longerUserName',1591696220),('m160408_100725_rename_groupadmin_to_manager',1591696220),('m160415_180332_wall_remove',1591696221),('m160501_220850_activity_pk_int',1591696222),('m160507_202611_settings',1591696222),('m160508_005740_settings_cleanup',1591696224),('m160509_214811_spaceurl',1591696224),('m160517_132535_group',1591696225),('m160523_105732_profile_searchable',1591696225),('m160714_142827_remove_space_id',1591696225),('m161031_161947_file_directories',1591696225),('m170110_151419_membership_notifications',1591696225),('m170110_152425_space_follow_reset_send_notification',1591696225),('m170111_190400_disable_web_notifications',1591696225),('m170112_115052_settings',1591696225),('m170118_162332_streamchannel',1591696226),('m170119_160740_initial',1591696226),('m170123_125622_pinned',1591696226),('m170211_105743_show_in_stream',1591696226),('m170224_100937_fix_default_modules',1591696226),('m170723_133337_content_tag',1591696228),('m170805_211208_authclient_id',1591696228),('m170810_220543_group_sort',1591696228),('m171015_155102_contentcontainer_module',1591696229),('m171025_142030_queue_update',1591696230),('m171025_200312_utf8mb4_fixes',1591696230),('m171027_220519_exclusive_jobs',1591696230),('m180305_084435_membership_pk',1591696232),('m180315_112748_fix_email_length',1591696233),('m190211_133045_channel_length',1591696233),('m190309_201944_rename_settings',1591696233);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_enabled`
--

DROP TABLE IF EXISTS `module_enabled`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_enabled` (
  `module_id` varchar(100) NOT NULL,
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_enabled`
--

LOCK TABLES `module_enabled` WRITE;
/*!40000 ALTER TABLE `module_enabled` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_enabled` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `seen` tinyint(4) DEFAULT NULL,
  `source_class` varchar(100) DEFAULT NULL,
  `source_pk` int(11) DEFAULT NULL,
  `space_id` int(11) DEFAULT NULL,
  `emailed` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `desktop_notified` tinyint(1) DEFAULT 0,
  `originator_user_id` int(11) DEFAULT NULL,
  `module` varchar(100) DEFAULT '',
  `group_key` varchar(75) DEFAULT NULL,
  `send_web_notifications` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `index_user_id` (`user_id`),
  KEY `index_seen` (`seen`),
  KEY `index_desktop_notified` (`desktop_notified`),
  KEY `index_desktop_emailed` (`emailed`),
  KEY `index_groupuser` (`user_id`,`class`,`group_key`),
  CONSTRAINT `fk_notification-user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification`
--

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_2trash` text DEFAULT NULL,
  `message` text DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile` (
  `user_id` int(11) NOT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `birthday_hide_year` int(1) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `about` text DEFAULT NULL,
  `phone_private` varchar(255) DEFAULT NULL,
  `phone_work` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `im_skype` varchar(255) DEFAULT NULL,
  `im_msn` varchar(255) DEFAULT NULL,
  `im_xmpp` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `url_facebook` varchar(255) DEFAULT NULL,
  `url_linkedin` varchar(255) DEFAULT NULL,
  `url_xing` varchar(255) DEFAULT NULL,
  `url_youtube` varchar(255) DEFAULT NULL,
  `url_vimeo` varchar(255) DEFAULT NULL,
  `url_flickr` varchar(255) DEFAULT NULL,
  `url_myspace` varchar(255) DEFAULT NULL,
  `url_googleplus` varchar(255) DEFAULT NULL,
  `url_twitter` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_profile-user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile`
--

LOCK TABLES `profile` WRITE;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` VALUES (1,'Sys','Admin','System Administration',NULL,NULL,NULL,NULL,'FR',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile_field`
--

DROP TABLE IF EXISTS `profile_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_field_category_id` int(11) NOT NULL,
  `module_id` varchar(255) DEFAULT NULL,
  `field_type_class` varchar(255) NOT NULL,
  `field_type_config` text DEFAULT NULL,
  `internal_name` varchar(100) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `sort_order` int(11) NOT NULL DEFAULT 100,
  `required` tinyint(4) DEFAULT NULL,
  `show_at_registration` tinyint(4) DEFAULT NULL,
  `editable` tinyint(4) NOT NULL DEFAULT 1,
  `visible` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `ldap_attribute` varchar(255) DEFAULT NULL,
  `translation_category` varchar(255) DEFAULT NULL,
  `is_system` int(1) DEFAULT NULL,
  `searchable` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `index_profile_field_category` (`profile_field_category_id`),
  CONSTRAINT `fk_profile_field-profile_field_category_id` FOREIGN KEY (`profile_field_category_id`) REFERENCES `profile_field_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile_field`
--

LOCK TABLES `profile_field` WRITE;
/*!40000 ALTER TABLE `profile_field` DISABLE KEYS */;
INSERT INTO `profile_field` VALUES (1,1,NULL,'humhub\\modules\\user\\models\\fieldtype\\Text','{\"minLength\":null,\"maxLength\":20,\"validator\":null,\"default\":null,\"regexp\":null,\"regexpErrorMessage\":null,\"fieldTypes\":[]}','firstname','First name',NULL,100,1,1,1,1,'2020-06-09 11:50:34',NULL,'2020-06-09 11:50:34',NULL,'givenName',NULL,1,1),(2,1,NULL,'humhub\\modules\\user\\models\\fieldtype\\Text','{\"minLength\":null,\"maxLength\":30,\"validator\":null,\"default\":null,\"regexp\":null,\"regexpErrorMessage\":null,\"fieldTypes\":[]}','lastname','Last name',NULL,200,1,1,1,1,'2020-06-09 11:50:34',NULL,'2020-06-09 11:50:34',NULL,'sn',NULL,1,1),(3,1,NULL,'humhub\\modules\\user\\models\\fieldtype\\Text','{\"minLength\":null,\"maxLength\":50,\"validator\":null,\"default\":null,\"regexp\":null,\"regexpErrorMessage\":null,\"fieldTypes\":[]}','title','Title',NULL,300,NULL,NULL,1,1,'2020-06-09 11:50:34',NULL,'2020-06-09 11:50:34',NULL,'title',NULL,1,1),(4,1,NULL,'humhub\\modules\\user\\models\\fieldtype\\Select','{\"options\":\"male=>Male\\nfemale=>Female\\ncustom=>Custom\",\"fieldTypes\":[]}','gender','Gender',NULL,300,NULL,NULL,1,1,'2020-06-09 11:50:34',NULL,'2020-06-09 11:50:34',NULL,NULL,NULL,1,1),(5,1,NULL,'humhub\\modules\\user\\models\\fieldtype\\Text','{\"minLength\":null,\"maxLength\":150,\"validator\":null,\"default\":null,\"regexp\":null,\"regexpErrorMessage\":null,\"fieldTypes\":[]}','street','Street',NULL,400,NULL,NULL,1,1,'2020-06-09 11:50:34',NULL,'2020-06-09 11:50:34',NULL,NULL,NULL,1,1),(6,1,NULL,'humhub\\modules\\user\\models\\fieldtype\\Text','{\"minLength\":null,\"maxLength\":10,\"validator\":null,\"default\":null,\"regexp\":null,\"regexpErrorMessage\":null,\"fieldTypes\":[]}','zip','Zip',NULL,500,NULL,NULL,1,1,'2020-06-09 11:50:34',NULL,'2020-06-09 11:50:34',NULL,NULL,NULL,1,1),(7,1,NULL,'humhub\\modules\\user\\models\\fieldtype\\Text','{\"minLength\":null,\"maxLength\":100,\"validator\":null,\"default\":null,\"regexp\":null,\"regexpErrorMessage\":null,\"fieldTypes\":[]}','city','City',NULL,600,NULL,NULL,1,1,'2020-06-09 11:50:34',NULL,'2020-06-09 11:50:34',NULL,NULL,NULL,1,1),(8,1,NULL,'humhub\\modules\\user\\models\\fieldtype\\CountrySelect','{\"options\":null,\"fieldTypes\":[]}','country','Country',NULL,700,NULL,NULL,1,1,'2020-06-09 11:50:34',NULL,'2020-06-09 11:50:34',NULL,NULL,NULL,1,1),(9,1,NULL,'humhub\\modules\\user\\models\\fieldtype\\Text','{\"minLength\":null,\"maxLength\":100,\"validator\":null,\"default\":null,\"regexp\":null,\"regexpErrorMessage\":null,\"fieldTypes\":[]}','state','State',NULL,800,NULL,NULL,1,1,'2020-06-09 11:50:34',NULL,'2020-06-09 11:50:34',NULL,NULL,NULL,1,1),(10,1,NULL,'humhub\\modules\\user\\models\\fieldtype\\Birthday','{\"defaultHideAge\":\"0\",\"fieldTypes\":[]}','birthday','Birthday',NULL,900,NULL,NULL,1,1,'2020-06-09 11:50:34',NULL,'2020-06-09 11:50:34',NULL,NULL,NULL,1,1),(11,1,NULL,'humhub\\modules\\user\\models\\fieldtype\\TextArea','{\"fieldTypes\":[]}','about','About',NULL,900,NULL,NULL,1,1,'2020-06-09 11:50:34',NULL,'2020-06-09 11:50:34',NULL,NULL,NULL,1,1),(12,2,NULL,'humhub\\modules\\user\\models\\fieldtype\\Text','{\"minLength\":null,\"maxLength\":100,\"validator\":null,\"default\":null,\"regexp\":null,\"regexpErrorMessage\":null,\"fieldTypes\":[]}','phone_private','Phone Private',NULL,100,NULL,NULL,1,1,'2020-06-09 11:50:34',NULL,'2020-06-09 11:50:34',NULL,NULL,NULL,1,1),(13,2,NULL,'humhub\\modules\\user\\models\\fieldtype\\Text','{\"minLength\":null,\"maxLength\":100,\"validator\":null,\"default\":null,\"regexp\":null,\"regexpErrorMessage\":null,\"fieldTypes\":[]}','phone_work','Phone Work',NULL,200,NULL,NULL,1,1,'2020-06-09 11:50:34',NULL,'2020-06-09 11:50:34',NULL,NULL,NULL,1,1),(14,2,NULL,'humhub\\modules\\user\\models\\fieldtype\\Text','{\"minLength\":null,\"maxLength\":100,\"validator\":null,\"default\":null,\"regexp\":null,\"regexpErrorMessage\":null,\"fieldTypes\":[]}','mobile','Mobile',NULL,300,NULL,NULL,1,1,'2020-06-09 11:50:34',NULL,'2020-06-09 11:50:34',NULL,NULL,NULL,1,1),(15,2,NULL,'humhub\\modules\\user\\models\\fieldtype\\Text','{\"minLength\":null,\"maxLength\":100,\"validator\":null,\"default\":null,\"regexp\":null,\"regexpErrorMessage\":null,\"fieldTypes\":[]}','fax','Fax',NULL,400,NULL,NULL,1,1,'2020-06-09 11:50:34',NULL,'2020-06-09 11:50:35',NULL,NULL,NULL,1,1),(16,2,NULL,'humhub\\modules\\user\\models\\fieldtype\\Text','{\"minLength\":null,\"maxLength\":100,\"validator\":null,\"default\":null,\"regexp\":null,\"regexpErrorMessage\":null,\"fieldTypes\":[]}','im_skype','Skype Nickname',NULL,500,NULL,NULL,1,1,'2020-06-09 11:50:35',NULL,'2020-06-09 11:50:35',NULL,NULL,NULL,1,1),(17,2,NULL,'humhub\\modules\\user\\models\\fieldtype\\Text','{\"minLength\":null,\"maxLength\":100,\"validator\":null,\"default\":null,\"regexp\":null,\"regexpErrorMessage\":null,\"fieldTypes\":[]}','im_msn','MSN',NULL,600,NULL,NULL,1,1,'2020-06-09 11:50:35',NULL,'2020-06-09 11:50:35',NULL,NULL,NULL,1,1),(18,2,NULL,'humhub\\modules\\user\\models\\fieldtype\\Text','{\"minLength\":null,\"maxLength\":255,\"validator\":\"email\",\"default\":null,\"regexp\":null,\"regexpErrorMessage\":null,\"fieldTypes\":[]}','im_xmpp','XMPP Jabber Address',NULL,800,NULL,NULL,1,1,'2020-06-09 11:50:35',NULL,'2020-06-09 11:50:35',NULL,NULL,NULL,1,1),(19,3,NULL,'humhub\\modules\\user\\models\\fieldtype\\Text','{\"minLength\":null,\"maxLength\":255,\"validator\":\"url\",\"default\":null,\"regexp\":null,\"regexpErrorMessage\":null,\"fieldTypes\":[]}','url','Url',NULL,100,NULL,NULL,1,1,'2020-06-09 11:50:35',NULL,'2020-06-09 11:50:35',NULL,NULL,NULL,1,1),(20,3,NULL,'humhub\\modules\\user\\models\\fieldtype\\Text','{\"minLength\":null,\"maxLength\":255,\"validator\":\"url\",\"default\":null,\"regexp\":null,\"regexpErrorMessage\":null,\"fieldTypes\":[]}','url_facebook','Facebook URL',NULL,200,NULL,NULL,1,1,'2020-06-09 11:50:35',NULL,'2020-06-09 11:50:35',NULL,NULL,NULL,1,1),(21,3,NULL,'humhub\\modules\\user\\models\\fieldtype\\Text','{\"minLength\":null,\"maxLength\":255,\"validator\":\"url\",\"default\":null,\"regexp\":null,\"regexpErrorMessage\":null,\"fieldTypes\":[]}','url_linkedin','LinkedIn URL',NULL,300,NULL,NULL,1,1,'2020-06-09 11:50:35',NULL,'2020-06-09 11:50:35',NULL,NULL,NULL,1,1),(22,3,NULL,'humhub\\modules\\user\\models\\fieldtype\\Text','{\"minLength\":null,\"maxLength\":255,\"validator\":\"url\",\"default\":null,\"regexp\":null,\"regexpErrorMessage\":null,\"fieldTypes\":[]}','url_xing','Xing URL',NULL,400,NULL,NULL,1,1,'2020-06-09 11:50:35',NULL,'2020-06-09 11:50:35',NULL,NULL,NULL,1,1),(23,3,NULL,'humhub\\modules\\user\\models\\fieldtype\\Text','{\"minLength\":null,\"maxLength\":255,\"validator\":\"url\",\"default\":null,\"regexp\":null,\"regexpErrorMessage\":null,\"fieldTypes\":[]}','url_youtube','Youtube URL',NULL,500,NULL,NULL,1,1,'2020-06-09 11:50:35',NULL,'2020-06-09 11:50:35',NULL,NULL,NULL,1,1),(24,3,NULL,'humhub\\modules\\user\\models\\fieldtype\\Text','{\"minLength\":null,\"maxLength\":255,\"validator\":\"url\",\"default\":null,\"regexp\":null,\"regexpErrorMessage\":null,\"fieldTypes\":[]}','url_vimeo','Vimeo URL',NULL,600,NULL,NULL,1,1,'2020-06-09 11:50:35',NULL,'2020-06-09 11:50:35',NULL,NULL,NULL,1,1),(25,3,NULL,'humhub\\modules\\user\\models\\fieldtype\\Text','{\"minLength\":null,\"maxLength\":255,\"validator\":\"url\",\"default\":null,\"regexp\":null,\"regexpErrorMessage\":null,\"fieldTypes\":[]}','url_flickr','Flickr URL',NULL,700,NULL,NULL,1,1,'2020-06-09 11:50:35',NULL,'2020-06-09 11:50:35',NULL,NULL,NULL,1,1),(26,3,NULL,'humhub\\modules\\user\\models\\fieldtype\\Text','{\"minLength\":null,\"maxLength\":255,\"validator\":\"url\",\"default\":null,\"regexp\":null,\"regexpErrorMessage\":null,\"fieldTypes\":[]}','url_myspace','MySpace URL',NULL,800,NULL,NULL,1,1,'2020-06-09 11:50:35',NULL,'2020-06-09 11:50:35',NULL,NULL,NULL,1,1),(27,3,NULL,'humhub\\modules\\user\\models\\fieldtype\\Text','{\"minLength\":null,\"maxLength\":255,\"validator\":\"url\",\"default\":null,\"regexp\":null,\"regexpErrorMessage\":null,\"fieldTypes\":[]}','url_googleplus','Google+ URL',NULL,900,NULL,NULL,1,1,'2020-06-09 11:50:35',NULL,'2020-06-09 11:50:35',NULL,NULL,NULL,1,1),(28,3,NULL,'humhub\\modules\\user\\models\\fieldtype\\Text','{\"minLength\":null,\"maxLength\":255,\"validator\":\"url\",\"default\":null,\"regexp\":null,\"regexpErrorMessage\":null,\"fieldTypes\":[]}','url_twitter','Twitter URL',NULL,1000,NULL,NULL,1,1,'2020-06-09 11:50:35',NULL,'2020-06-09 11:50:35',NULL,NULL,NULL,1,1);
/*!40000 ALTER TABLE `profile_field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile_field_category`
--

DROP TABLE IF EXISTS `profile_field_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile_field_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT 100,
  `module_id` int(11) DEFAULT NULL,
  `visibility` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `translation_category` varchar(255) DEFAULT NULL,
  `is_system` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile_field_category`
--

LOCK TABLES `profile_field_category` WRITE;
/*!40000 ALTER TABLE `profile_field_category` DISABLE KEYS */;
INSERT INTO `profile_field_category` VALUES (1,'General','',100,NULL,1,'2020-06-09 11:50:33',NULL,'2020-06-09 11:50:33',NULL,NULL,1),(2,'Communication','',200,NULL,1,'2020-06-09 11:50:33',NULL,'2020-06-09 11:50:33',NULL,NULL,1),(3,'Social bookmarks','',300,NULL,1,'2020-06-09 11:50:33',NULL,'2020-06-09 11:50:33',NULL,NULL,1);
/*!40000 ALTER TABLE `profile_field_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `queue`
--

DROP TABLE IF EXISTS `queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel` varchar(50) NOT NULL,
  `job` blob NOT NULL,
  `pushed_at` int(11) NOT NULL,
  `ttr` int(11) NOT NULL,
  `delay` int(11) NOT NULL,
  `priority` int(11) unsigned NOT NULL DEFAULT 1024,
  `reserved_at` int(11) DEFAULT NULL,
  `attempt` int(11) DEFAULT NULL,
  `done_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `channel` (`channel`),
  KEY `reserved_at` (`reserved_at`),
  KEY `priority` (`priority`)
) ENGINE=InnoDB AUTO_INCREMENT=699 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `queue`
--

LOCK TABLES `queue` WRITE;
/*!40000 ALTER TABLE `queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `queue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `queue_exclusive`
--

DROP TABLE IF EXISTS `queue_exclusive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `queue_exclusive` (
  `id` varchar(50) NOT NULL,
  `job_message_id` varchar(50) DEFAULT NULL,
  `job_status` smallint(6) DEFAULT 2,
  `last_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `queue_exclusive`
--

LOCK TABLES `queue_exclusive` WRITE;
/*!40000 ALTER TABLE `queue_exclusive` DISABLE KEYS */;
INSERT INTO `queue_exclusive` VALUES ('search.update.a4cb70bb5efe4d31fd8302088ec03b1d','7',2,'2020-06-09 09:56:37');
/*!40000 ALTER TABLE `queue_exclusive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `value` text DEFAULT NULL,
  `module_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `unique-setting` (`name`,`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `setting`
--

LOCK TABLES `setting` WRITE;
/*!40000 ALTER TABLE `setting` DISABLE KEYS */;
INSERT INTO `setting` VALUES (1,'oembedProviders','{\"vimeo.com\":\"http:\\/\\/vimeo.com\\/api\\/oembed.json?scheme=https&url=%url%&format=json&maxwidth=450\",\"youtube.com\":\"http:\\/\\/www.youtube.com\\/oembed?scheme=https&url=%url%&format=json&maxwidth=450\",\"youtu.be\":\"http:\\/\\/www.youtube.com\\/oembed?scheme=https&url=%url%&format=json&maxwidth=450\",\"soundcloud.com\":\"https:\\/\\/soundcloud.com\\/oembed?url=%url%&format=json&maxwidth=450\",\"slideshare.net\":\"https:\\/\\/www.slideshare.net\\/api\\/oembed\\/2?url=%url%&format=json&maxwidth=450\"}','base'),(2,'defaultVisibility','1','space'),(3,'defaultJoinPolicy','1','space'),(4,'baseUrl','http://localhost:80http://localhost:80','base'),(5,'paginationSize','10','base'),(6,'displayNameFormat','{profile.firstname} {profile.lastname}','base'),(7,'horImageScrollOnMobile','1','base'),(8,'cronLastDailyRun','1592244000','base'),(9,'auth.needApproval','0','user'),(10,'auth.anonymousRegistration','1','user'),(11,'auth.internalUsersCanInvite','1','user'),(12,'mailer.transportType','php','base'),(13,'mailer.systemEmailAddress','social@example.com','base'),(14,'mailer.systemEmailName','humhub@example.com','base'),(15,'mailSummaryInterval','2','activity'),(16,'maxFileSize','5242880','file'),(17,'maxPreviewImageWidth','200','file'),(18,'maxPreviewImageHeight','200','file'),(19,'hideImageFileInfo','0','file'),(20,'cache.class','yii\\caching\\FileCache','base'),(21,'cache.expireTime','3600','base'),(22,'installationId','0fad267d69f25632c36d728e4925ddd3','admin'),(23,'spaceOrder','0','space'),(24,'enable','1','tour'),(25,'defaultLanguage','fr','base'),(26,'enable_html5_desktop_notifications','0','notification'),(27,'cronLastRun','1592300580','base'),(28,'name','HumHub','base'),(29,'secret','918faa0f-7bc6-4e10-b909-35edd29b5f22','base'),(30,'timeZone','UTC','base'),(31,'cronLastHourlyRun','1592299200','base'),(32,'themeParents','[]','base'),(33,'group.adminGroupId','1','user'),(34,'showProfilePostForm','0','dashboard'),(35,'enable','0','friendship'),(36,'defaultSort','c','stream'),(37,'lastVersionNotify','1.5.2','admin'),(38,'theme.var.HumHub.default','#ededed','base'),(39,'theme.var.HumHub.primary','#708fa0','base'),(40,'theme.var.HumHub.info','#6fdbe8','base'),(41,'theme.var.HumHub.success','#97d271','base'),(42,'theme.var.HumHub.warning','#fdd198','base'),(43,'theme.var.HumHub.danger','#ff8989','base'),(44,'theme.var.HumHub.text-color-main','#777','base'),(45,'theme.var.HumHub.text-color-secondary','#7a7a7a','base'),(46,'theme.var.HumHub.text-color-highlight','#555','base'),(47,'theme.var.HumHub.text-color-soft','#bebebe','base'),(48,'theme.var.HumHub.text-color-soft2','#aeaeae','base'),(49,'theme.var.HumHub.text-color-soft3','#bac2c7','base'),(50,'theme.var.HumHub.text-color-contrast','#fff','base'),(51,'theme.var.HumHub.background-color-main','#fff','base'),(52,'theme.var.HumHub.background-color-secondary','#f7f7f7','base'),(53,'theme.var.HumHub.background-color-page','#ededed','base'),(54,'theme.var.HumHub.background-color-highlight','#fff8e0','base'),(55,'theme.var.HumHub.background3','#d7d7d7','base'),(56,'theme.var.HumHub.background4','#b2b2b2','base'),(57,'theme.var.HumHub.background-color-success','#f7fbf4','base'),(58,'theme.var.HumHub.text-color-success','#84be5e','base'),(59,'theme.var.HumHub.border-color-success','#97d271','base'),(60,'theme.var.HumHub.background-color-warning','#fffbf7','base'),(61,'theme.var.HumHub.text-color-warning','#e9b168','base'),(62,'theme.var.HumHub.border-color-warning','#fdd198','base'),(63,'theme.var.HumHub.background-color-danger','#fff6f6','base'),(64,'theme.var.HumHub.text-color-danger',' #ff8989','base'),(65,'theme.var.HumHub.border-color-danger','#ff8989','base'),(66,'theme.var.HumHub.mail-font-url','\'http://fonts.googleapis.com/css?family=Open+Sans:300,100,400,600\'','base'),(67,'theme.var.HumHub.mail-font-family','\'Open Sans,Arial,Tahoma,Helvetica,sans-serif\'','base');
/*!40000 ALTER TABLE `setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `space`
--

DROP TABLE IF EXISTS `space`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `space` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` varchar(45) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `description` text DEFAULT NULL,
  `join_policy` tinyint(4) DEFAULT NULL,
  `visibility` tinyint(4) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `tags` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `ldap_dn` varchar(255) DEFAULT NULL,
  `auto_add_new_members` int(4) DEFAULT NULL,
  `contentcontainer_id` int(11) DEFAULT NULL,
  `default_content_visibility` tinyint(1) DEFAULT NULL,
  `color` varchar(7) DEFAULT NULL,
  `members_can_leave` int(11) DEFAULT 1,
  `url` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url-unique` (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `space`
--

LOCK TABLES `space` WRITE;
/*!40000 ALTER TABLE `space` DISABLE KEYS */;
/*!40000 ALTER TABLE `space` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `space_membership`
--

DROP TABLE IF EXISTS `space_membership`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `space_membership` (
  `space_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `originator_user_id` varchar(45) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `request_message` text DEFAULT NULL,
  `last_visit` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `group_id` varchar(255) DEFAULT 'member',
  `show_at_dashboard` tinyint(1) DEFAULT 1,
  `can_cancel_membership` int(11) DEFAULT 1,
  `send_notifications` tinyint(1) DEFAULT 0,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`),
  KEY `fk_space_membership-space_id` (`space_id`),
  KEY `fk_space_membership-user_id` (`user_id`),
  CONSTRAINT `fk_space_membership-space_id` FOREIGN KEY (`space_id`) REFERENCES `space` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_space_membership-user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `space_membership`
--

LOCK TABLES `space_membership` WRITE;
/*!40000 ALTER TABLE `space_membership` DISABLE KEYS */;
/*!40000 ALTER TABLE `space_membership` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `url_oembed`
--

DROP TABLE IF EXISTS `url_oembed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `url_oembed` (
  `url` varchar(180) NOT NULL,
  `preview` text NOT NULL,
  PRIMARY KEY (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `url_oembed`
--

LOCK TABLES `url_oembed` WRITE;
/*!40000 ALTER TABLE `url_oembed` DISABLE KEYS */;
/*!40000 ALTER TABLE `url_oembed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` varchar(45) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `email` char(150) DEFAULT NULL,
  `auth_mode` varchar(10) NOT NULL,
  `tags` text DEFAULT NULL,
  `language` varchar(5) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `visibility` int(1) DEFAULT 1,
  `time_zone` varchar(100) DEFAULT NULL,
  `contentcontainer_id` int(11) DEFAULT NULL,
  `authclient_id` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_email` (`email`),
  UNIQUE KEY `unique_username` (`username`),
  UNIQUE KEY `unique_guid` (`guid`),
  UNIQUE KEY `unique_authclient_id` (`authclient_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'d0b68d72-03a2-4bd1-98dc-3e5087662589',1,'admin','humhub@example.com','local','','','2020-06-09 11:50:36',NULL,'2020-06-09 11:56:37',1,'2020-06-12 10:52:49',1,'UTC',1,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_auth`
--

DROP TABLE IF EXISTS `user_auth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_auth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `source` varchar(255) NOT NULL,
  `source_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_id` (`user_id`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_auth`
--

LOCK TABLES `user_auth` WRITE;
/*!40000 ALTER TABLE `user_auth` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_auth` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_follow`
--

DROP TABLE IF EXISTS `user_follow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_follow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_model` varchar(100) NOT NULL,
  `object_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `send_notifications` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `index_user` (`user_id`),
  KEY `index_object` (`object_model`,`object_id`),
  CONSTRAINT `fk_user_follow-user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_follow`
--

LOCK TABLES `user_follow` WRITE;
/*!40000 ALTER TABLE `user_follow` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_follow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_friendship`
--

DROP TABLE IF EXISTS `user_friendship`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_friendship` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `friend_user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx-friends` (`user_id`,`friend_user_id`),
  KEY `fk-friend` (`friend_user_id`),
  CONSTRAINT `fk-friend` FOREIGN KEY (`friend_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk-user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_friendship`
--

LOCK TABLES `user_friendship` WRITE;
/*!40000 ALTER TABLE `user_friendship` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_friendship` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_http_session`
--

DROP TABLE IF EXISTS `user_http_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_http_session` (
  `id` char(64) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `data` longblob DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_http_session-user_id` (`user_id`),
  CONSTRAINT `fk_user_http_session-user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_http_session`
--

LOCK TABLES `user_http_session` WRITE;
/*!40000 ALTER TABLE `user_http_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_http_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_invite`
--

DROP TABLE IF EXISTS `user_invite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_invite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_originator_id` int(11) DEFAULT NULL,
  `space_invite_id` int(11) DEFAULT NULL,
  `email` char(150) NOT NULL,
  `source` varchar(45) DEFAULT NULL,
  `token` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `language` varchar(10) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_email` (`email`),
  UNIQUE KEY `unique_token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_invite`
--

LOCK TABLES `user_invite` WRITE;
/*!40000 ALTER TABLE `user_invite` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_invite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_mentioning`
--

DROP TABLE IF EXISTS `user_mentioning`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_mentioning` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_model` varchar(100) NOT NULL,
  `object_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `i_user` (`user_id`),
  KEY `i_object` (`object_model`,`object_id`),
  CONSTRAINT `fk_user_mentioning-user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_mentioning`
--

LOCK TABLES `user_mentioning` WRITE;
/*!40000 ALTER TABLE `user_mentioning` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_mentioning` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_password`
--

DROP TABLE IF EXISTS `user_password`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_password` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `algorithm` varchar(20) DEFAULT NULL,
  `password` text DEFAULT NULL,
  `salt` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`),
  CONSTRAINT `fk_user_password-user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_password`
--

LOCK TABLES `user_password` WRITE;
/*!40000 ALTER TABLE `user_password` DISABLE KEYS */;
INSERT INTO `user_password` VALUES (1,1,'sha512whirlpool','33592d90f5352a968cff18fba02c71c3c1855ebc6178e0c85d67716f799e974c8e8cd11838d9d94331b4a1be9cd30b9b844f79c1260af2579f678626ed21a977','c6d7298d-26fb-4910-b45d-a970aaebdd56','2020-06-09 11:50:36');
/*!40000 ALTER TABLE `user_password` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-16 11:43:09
